import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ParticipationComponent } from './participation/participation.component';
import { EngagementComponent } from './engagement/engagement.component';
import { RetentionComponent } from './retention/retention.component';

const routes: Routes = [
  //{path:'', component:AppComponent},
 // {path:"dashboard", component:DashboardComponent},
  {path:"participation", component:ParticipationComponent},
  {path:"engagement", component:EngagementComponent},
  {path:"retention", component:RetentionComponent},
  {path:"**", redirectTo:''}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
