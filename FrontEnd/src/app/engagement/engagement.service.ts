import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Participation, EngagementOutput } from '../models/Participation';
import { MockValue1 } from '../models/mocklist';


@Injectable({
  providedIn: 'root'
})
export class EngagementService {

  constructor(private _http: HttpClient) { }
  mockValue= MockValue1;


  private url = 'http://localhost:3000/engage'; 
  
    //  private url = 'https://api.myjson.com/bins/cz1u5'; 
  
   getEngagementData(participation: Participation)
            : Observable < EngagementOutput > {             
                   return this._http.post<EngagementOutput>(`${this.url}`,participation)
                          
  }

  


}

