import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Participation, EngagementOutput, buChart } from '../models/participation';
import { EngagementService } from './engagement.service';

@Component({
  selector: 'app-engagement',
  templateUrl: './engagement.component.html',
  styleUrls: ['./engagement.component.css']
})
export class EngagementComponent implements OnInit {

  constructor(private _engagementService: EngagementService) { }

  ngOnInit() {
  }

  private participation: Participation;
  engagementOutput: EngagementOutput;
  private error: Error;
  buChart: buChart;
  pieChartData: any[] = [];
  //arr : any[][];
  public arrayOut: any[];
  // arrOut = [];

  busUnit: any = ['MLEU', "Health Care", "Banking", "Entertainment", "Field Maarketing", "AVM", "Quality Engineering & Assurance"];
  baLocation: any = ['Chennai', "Singapore", "Pune", "Coimbatore", "United Kingdom", "Coimbatore"];
  fArea: any = ["Donation or Distribution", "Be a Teacher", "Community Program", "Bags Of Joy Distribution", "BAT", "Teaching", "Improve in Co-ordination"];

  engagementForm = new FormGroup({

    businessUnit: new FormControl("All"),
    baseLocation: new FormControl("All"),
    focusArea: new FormControl("All")
  })

  onFilterSubmit() {
    console.log(this.engagementForm.value.businessUnit);
    this.participation = this.engagementForm.value;
    console.log(this.participation);
    this._engagementService.getEngagementData(this.participation)
      .subscribe(
      (data) => {
        if (data) {
          this.engagementOutput = data;
          console.log(this.engagementOutput);
          const arr: any[][] = [["oneTime", this.engagementOutput.oneTime],
          ["twoPlusTime", this.engagementOutput.twoPlusTime],
          ["fivePlusTime", this.engagementOutput.fivePlusTime]];
          console.log(arr);
          this.chartData = arr;
        }
        else {
          console.log("No value recevied");
        }
      },
      (error) => {
        this.error = error;
        console.log(this.error);
        console.log("returned from the error service")
      },
    )




  }


  //arrOut = this.engagementOutput;



  chartTitle = "BU wise Comparision";
  chartType = 'BarChart';

  chartData: any;

  columnNames = ['Times', 'percentage'];
  Chartoptions = {
  };
  width = 550;
  height = 400;

}








