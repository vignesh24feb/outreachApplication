import { Participation, ParticipationOutput, EngagementOutput } from './Participation';


export const MockValue : ParticipationOutput[] =  [
    {
        associates  : 20,
        volunteers: 25,
        volunteerhrs : 2,
        coverage  : 3,
        avgFreqPerVounteer  : 2,
        avgHrsPerAssociate  : 2,
        avgHrsPerVounteer : 2,
        events: 34,
        avgHrsWeekDay : 45,
        avgHrsWeekEnd : 4,
        avgVoulnteers : 23,
        avgHrsVoulnteered: 23
    }]

    export const MockValue1 : EngagementOutput[] =  [
        {
            oneTime  : 20,
            twoPlusTime: 80,
            fivePlusTime : 10 
        }]
    
