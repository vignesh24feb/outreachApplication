export class Participation  {
    businessUnit  : string;
    baseLocation: string;
    focusArea : string;
    fromDate  : string;
    toDate  : string;
  }
  

  export class ParticipationOutput  {
    associates  : number;
    volunteers: number;
    volunteerhrs : number;
    coverage  : number;
    avgFreqPerVounteer  : number;
    avgHrsPerAssociate  : number;
    avgHrsPerVounteer : number;
    events: number;
    avgHrsWeekDay : number;
    avgHrsWeekEnd : number;
    avgVoulnteers : number;
    avgHrsVoulnteered: number
  }

export class EngagementOutput  {
   oneTime  : number;
   twoPlusTime: number;
    fivePlusTime : number;    
   
  }
      
  export class buChart {
    _id : string;
    buCount : number
  }
  //export buChart : any[][];
  
 // export var arr = [];