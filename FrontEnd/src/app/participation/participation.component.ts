import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Participation, ParticipationOutput, buChart } from '../models/Participation';
import { ParticipationService } from './participation.service';
import { PartbuchartService } from './partbuchart.service';
@Component({

  selector: 'app-participation',
  templateUrl: './participation.component.html',
  styleUrls: ['./participation.component.css']
})
export class ParticipationComponent implements OnInit {

  public arr1: any[] = [];
  arr2: any[][] = [[]];
  public test: string[] = ["1", '2', '3']
  buChart: buChart;
  public arrayOut: any[];

  constructor(private _participationService: ParticipationService,
    private _PartbuchartService: PartbuchartService) { }

  ngOnInit() {

  }


  private participation: Participation;
  participationOutput: ParticipationOutput;
  private error: Error;


  busUnit: any = ['MLEU', "Health Care", "Banking", "Entertainment", "Field Maarketing", "AVM", "Quality Engineering & Assurance"];
  baLocation: any = ['Chennai', "Singapore", "Pune", "Coimbatore", "United Kingdom", "Coimbatore"];
  fArea: any = ["Donation or Distribution", "Be a Teacher", "Community Program", "Bags Of Joy Distribution", "BAT", "Teaching", "Improve in Co-ordination"];

  participationForm = new FormGroup({

    businessUnit: new FormControl("All"),
    baseLocation: new FormControl("All"),
    focusArea: new FormControl("All"),
  })

  onFilterSubmit() {
    var pieChartData: any[] = [];
    console.log(this.participationForm.value.businessUnit);
    this.participation = this.participationForm.value;
    console.log(this.participation);
    this._participationService.getParticipationData(this.participation)
      .subscribe(
      (data) => {
        if (data) {
          this.participationOutput = data;
          console.log(this.participationOutput);
          this._PartbuchartService.getBuChartData()
            .subscribe(
            (data1) => {
              if (data1) {

                this.arrayOut = [];
                //console.log( JSON.parse( data1));
                const array: any[] = data1;

                console.log(array)

                array.forEach(element => {
                  const arr = [];
                  arr.push(element._id);
                  arr.push(element.buCount);
                  this.arrayOut.push(arr);
                });

                console.log(this.arrayOut);

                this.chartData = this.arrayOut;


                //    this.arr1 = data1;
                //  const arrayOut =[];
                //  data1.forEach(element => {
                //    const array = [];
                //    array.push(element._id);
                //    array.push(element.buCount);
                //    arrayOut.push(array);

                //  })

                //      console.log(data1);
                //      console.log("array")
                //      console.log(pieChartData)
              }
              else {
                console.log("No value recevied");
              }
            },
            (error) => {
              this.error = error;
              console.log(this.error);
              console.log("returned from the error service")
            },
          )
        }
        else {
          console.log("No value recevied");
        }
      },
      (error) => {
        this.error = error;
        console.log(this.error);
        console.log("returned from the error service")
      },
    )
    console.log("arr2 " + pieChartData);
    //console.log(google.visualization.arrayToDataTable(pieChartData))

  }


  chartTitle = "BU wise Comparision";

  chartType = 'PieChart';

  chartData: any; // =this.arrayOut; //"pieChartData[0]";


  /*chartData = [
    ['Firefox', 45.0],
    ['IE', 26.8],
    ['Chrome', 12.8],
    ['Safari', 8.5],
    ['Opera', 6.2],
    ['Others', 0.7] 
 ];*/
  columnNames = ['BU', 'Percentage'];
  Chartoptions = {
  };
  width = 550;
  height = 400;

}