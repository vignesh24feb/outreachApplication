import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { buChart } from '../models/Participation';



@Injectable({
  providedIn: 'root'
})
export class PartbuchartService {

  constructor(private _http: HttpClient) { }

  private url = 'http://localhost:3000/buchart'; 
  
    //  private url = 'https://api.myjson.com/bins/cz1u5'; 
  
          getBuChartData()
                      : Observable < buChart[] > {
                    return this._http.get<buChart[]>(`${this.url}`)
                          
  }

 
  /*getParticipationData(participation: Participation): ParticipationOutput[]{
    console.log("Service called");
    console.log(participation);
    console.log(this.mockValue);
    return this.mockValue;
  }*/
}
