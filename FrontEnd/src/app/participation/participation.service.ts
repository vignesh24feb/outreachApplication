import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Participation, ParticipationOutput } from '../models/Participation';
import { MockValue } from '../models/mocklist';


@Injectable({
  providedIn: 'root'
})
export class ParticipationService {

  constructor(private _http: HttpClient) { }
  mockValue= MockValue;


  private url = 'http://localhost:3000/asso'; 
  
    //  private url = 'https://api.myjson.com/bins/cz1u5'; 
  
          getParticipationData(participation: Participation)
            : Observable < ParticipationOutput > {             
                   return this._http.post<ParticipationOutput>(`${this.url}`,participation)
                          
  }

 
  /*getParticipationData(participation: Participation): ParticipationOutput[]{
    console.log("Service called");
    console.log(participation);
    console.log(this.mockValue);
    return this.mockValue;
  }*/
}
