var mongoose = require ('mongoose');

var Schema = mongoose.Schema;

var assoDetials = new Schema({
    AssociateID: Number,
    Name :  String,
    Designation :  String,
    Location : String,
    BU : String
});

/*var OESSchema = new Schema({
    OES_Event_ID: String,
    OES_Month: String,
    OES_Base_Location: String,
    OES_Beneficiary_Name: String,
    OES_Venue_Address: String,
    OES_Council_Name: String,
    OES_Project: String,
    OES_Category: String,
    OES_Event_Name:String,
    OES_Event_Description: String,
    OES_Event_Date: Date,
    OES_Total_No_Of_Volunteers: Number,
    OES_Total_Volunteer_Hours: Number,
    OES_Total_Travel_Hours: Number,
    OES_Overall_Volunteering_Hours: Number,
    OES_Lives_Impacted: Number,
    OES_Activity_Type: Number,
    OES_Status: String,
    OES_POC_ID: Number,
    OES_POC_Name: String,
    OES_POC_Contact_Number: Number
});*/
mongoose.pluralize(null);
var AssoModel = mongoose.model('associateDetails',assoDetials);
//var OESModel = mongoose.model('OutreachEventSummary',OESSchema);

module.exports= AssoModel;
//module.exports= OESModel;
