//THIS MODULE HELPSS TO PERRFORM READ, UPDATE, DELETE 
//LOGIN DATABASE

module.exports = function (app) {
    var AssoModel = require('./assoDetails');
    var OESModel = require('./OESSchema');
    var OEIModel = require('./OEISchema');
    var query1 = require('./query1');
    // var bcrypt = require('bcryptjs');
    var bodyParser = require('body-parser');
    var jsonParser = bodyParser.json();
    var urlencodedParser = bodyParser.urlencoded({ extended: true });

    var Output = {
        associates: 0,
        volunteers: 0,
        volunteerhrs: 0,
        coverage: 0,
        avgHrsPerAssociate: 0,
        avgHrsPerVolunteer: 0,
        events: 0,
        avgHrsWeekDay: 0,
        avgHrsWeekEnd: 0,
        avgVoulnteers: 0,
        avgHrsVoulnteered: 0
    }

 var engageOutput = {
            oneTime  : 20,
            twoPlusTime: 80,
            fivePlusTime : 10 
 }
 var engageOutput1 = {
     engageTimes :  "",
     engageValue : 0
}


 var engageCount = {}

    var buChart = {
        bu : "",
        buCount : 0
    }

    var Volhours = {
        VH_evenId: 0,
        VH_day: 0,
        VH_hours: 0,
    }
    var totalVoulnteer = 0;
    var outputBool = false;
    var totalHrsWeekDay = 0;
    var totalHrsWeekEnd = 0;
    var countWeekDay = 0;
    var countWeekEnd = 0;
    var oneTimer = 0;
    var twoPlusTimer = 0;
    var fivePlusTimer = 0;


    console.log("Into the export");
    app.use("/", jsonParser, function (req, res, next) {
        query1(req);
        next();
    })


    app.post("/asso", jsonParser, function (req, res, next) {
        //query1(req);
        console.log("Into the Associate module");
        console.log(req.body);
        console.log(req.url);


        console.log(ADquery);
        //Query to find the Total Associate
        function queryfn() {
            AssoModel.countDocuments(ADquery, function (err, result) {
                if (err) throw err;
                Output.associates = result;
                console.log("Total Associate: " + Output.associates);
            });
            //Unique Volunteers
            console.log("Executing the second query");
            console.log(OEIQuery);

            OEIModel.find(OEIQuery).distinct("OEI_Employee_ID").countDocuments(function (err, result) {
                if (err) throw err;
                Output.volunteers = result
                console.log("unique volunteers:  " + Output.volunteers);

            });
            // Volunteers
            OEIModel.find(OEIQuery).countDocuments(function (err, result) {
                if (err) throw err;
                totalVoulnteer = result
                console.log("Total volunteers:  " + totalVoulnteer);
            });

            OEIModel.find(OEIQuery).distinct("OEI_Event_ID").countDocuments(function (err, result) {
                if (err) throw err;
                Output.events = result
                console.log("Number of Events:  " + Output.events);

            });

            // Total volunteering Hours   

            OEIModel.aggregate([
                { $match: OEIQuery }, { $group: { _id: null, sum: { $sum: "$OEI_Volunteer_Hours" }, sum2: { $sum: "$OEI_Travel_Hours" } } }
            ], function (err, result) {
                if (err) throw err;
                Output.volunteerhrs = result[0].sum + result[0].sum2
                console.log("VounteerHrs: " + Output.volunteerhrs);

            });

            OESModel.aggregate([
                { $match: OESQuery }, { $group: { _id: null, avg: { $avg: "$OES_Total_No_Of_Volunteers" } } }
            ], function (err, result) {
                if (err) throw err;
                Output.volunteerhrs = result[0].avg
                console.log(Output.volunteerhrs);

            });
            OESModel.aggregate([
                { $match: OESQuery },
                { $group: { _id: null, avg: { $avg: "$OES_Total_No_Of_Volunteers" } } }
            ], function (err, result) {
                if (err) throw err;
                Output.avgVoulnteers = result[0].avg
                console.log(Output.avgVoulnteers);

            });

            OESModel.aggregate([
                { $match: OESQuery },
                {
                    $project: {
                        _id: "$OES_Event_ID",
                        div: { $divide: ["$OES_Total_Volunteer_Hours", "$OES_Total_No_Of_Volunteers"] }
                    }
                },
                {
                    $group: {
                        _id: "_null",
                        avg: {
                            $avg: "$div"
                        },

                    }
                },
            ], function (err, result) {
                if (err) throw err;
                Output.avgHrsVoulnteered = result[0].avg
                console.log(result);
                console.log("Output.avgHrsVoulnteered" + Output.avgHrsVoulnteered);

            });

            OESModel.aggregate([
                { $match: OESQuery },
                {
                    $project: {
                        _id: "$OES_Event_ID",
                        day: { $dayOfWeek: ["$OES_Event_Date"] }, hours: "$OES_Total_Volunteer_Hours"
                    }
                },
            ], function (err, result) {
                if (err) throw err;
                console.log("day:")
                console.log(result)
                Volhours = result
                console.log(result[0].day);
                for (i = 0; i < result.length; i++) {
                    if (result[i].day == 1 || result[i].day == 7) {
                        totalHrsWeekDay = totalHrsWeekDay + result[i].hours
                        countWeekDay = countWeekDay + 1
                    }
                    else {
                        totalHrsWeekEnd = totalHrsWeekEnd + result[i].hours
                        countWeekEnd = countWeekEnd + 1
                    }
                }

                Output.avgHrsWeekDay = totalHrsWeekDay / countWeekDay;
                Output.avgHrsWeekEnd = totalHrsWeekEnd / countWeekEnd;

            });
            setTimeout(computation, 2400)
        }

        queryfn();



        function computation() {
            //Coverage
            Output.coverage =Output.volunteers / Output.associates
            console.log("Coverage:  " + Output.coverage);
            //Average Frequency per voulunteer
            Output.avgFreqPerVounteer = Output.volunteers / totalVoulnteer;
            console.log("Output.avgFreqPerVounteer: " + Output.avgFreqPerVounteer);
            //Average Hours per ssociate
            Output.avgHrsPerAssociate = Output.volunteerhrs / Output.associates;
            console.log("Output.avgHrsPerAssociat: " + Output.avgHrsPerAssociate);
            // Average hours per voulunteer.
            Output.avgHrsPerVounteer = Output.volunteerhrs / Output.volunteers
            console.log("Output.avgHrsPerAssociat: " + Output.avgHrsPerVounteer);

            console.log(Output);
            res.send(Output)
        }



    })
    
    app.get("/buchart",  function (req, res, next) {
        OEIModel.aggregate([
            {$match: {}}, 
            { $group: { _id: "$OEI_Business_Unit", buCount: { $sum: 1}} }
        ], function (err, result) {
            if (err) throw err;
            console.log(result);
            buChart = result;
            res.send(buChart)

        });

    })

    app.post("/engage", jsonParser, function (req, res, next) {
        console.log("Into the Engagement module");
        console.log(req.body);
        console.log(req.url);
        console.log(OEIQuery);
    
        OEIModel.aggregate([
            { $match: OEIQuery }, { $group: { _id: "$OEI_Employee_ID", count: { $sum: 1 } }}
        ], function (err, result) {
            if (err) throw err;     
           engageCount = result;

          oneTimer = 0
          twoPlusTimer = 0
          fivePlusTimer = 0
           engageCount.forEach((data) => {
               console.log(data)
               if(data.count == 1){
                   oneTimer = oneTimer + 1
               }
               else if(data.count > 1 && data.count < 5)
               {
                twoPlusTimer = twoPlusTimer +1;
               }
               else {
                fivePlusTimer = fivePlusTimer + 1;
               }

           })
           console.log("oneTimer"+ oneTimer)
           console.log("twoPlusTimer"+ twoPlusTimer)
           console.log("fivePlusTimer"+ fivePlusTimer)           
           engageOutput.oneTime = oneTimer
           engageOutput.twoPlusTime = twoPlusTimer
           engageOutput.fivePlusTime = fivePlusTimer
           console.log(engageOutput)
           res.send(engageOutput); 
        });
    })



}


