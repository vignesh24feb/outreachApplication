var mongoose = require ('mongoose');

var Schema = mongoose.Schema;

var OEISchema = new Schema({
    OEI_Event_ID: String,
    OEI_Base_Location: String,
    OEI_Beneficiary_Name: String,
    OEI_Council_Name: String,
    OEI_Event_Name: String,
    OEI_Event_Description: String,
    OEI_Event_Date: Date,
    OEI_Employee_ID: Number,
    OEI_Employee_Name: String,
    OEI_Volunteer_Hours: Number,
    OEI_Travel_Hours: Number,
    OEI_Lives_Impacted: Number,
    OEI_Business_Unit: String,
    OEI_Status: String,
    OEI_IIEP_Category: String,
});


mongoose.pluralize(null);
var OEIModel = mongoose.model('OutreachEventInfo',OEISchema);


module.exports= OEIModel;